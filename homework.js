﻿var currencyData = [];

// конструктор для новой валюты
function NewCurr(charCode, name, value) {
    this.charCode = charCode;
    this.name = name;
    this.value = value;
}

function appendCurrCode(code) {
    $('.init-currency__select').append('<option>'+code+'</option>');
    $('.new-currency__select').append('<option>'+code+'</option>');
}

function getCurrency() {
    $.get('http://university.netology.ru/api/currency', function(data) {

        // Заполняем массив currencyData и заполняем input кодами валют
        data.forEach(function(item) {
            var currObj = new NewCurr(item.CharCode, item.Name, item.Value);
            currencyData.push(currObj);
            appendCurrCode(currObj.charCode);
        });

        // Устанавливаем дефолтные занчения и вызываем для них функцию конвертации, вызываем обработчики
        init();
    });
}

function init() {
    $('.init-currency__select option:first').attr('selected', true);
    $('.new-currency__select option:first').attr('selected', true);
    countResult('original');

    // событие ввода в инпут
    $('.init-currency__input').keyup(function(event) {
        var newVal = $('.init-currency__input').val().replace(/\.(?=.*\.)|[^\d\.-]/g, '');
        $('.init-currency__input').val(newVal);
        countResult('original');
    });

    $('.new-currency__input').keyup(function(event) {
        if (isNumbOrSpecSymbol(event) === true) {
            countResult('reverse');
        } else {
            event.preventDefault();
            var newVal = $('.new-currency__input').val().replace(/\D/, '');
            $('.new-currency__input').val(newVal);
        }
    });

    // событие изменения селкта
    $('.init-currency__select').change(function() {
        countResult('original');
    });

    $('.new-currency__select').change(function() {
        countResult('reverse');
    });

    // Событие для ссылки добавить валюту
    $('.add-currency__link').click(function() {
        var addCurrForm = $('.add-currency');
        if (addCurrForm.css('visibility') == 'hidden') {
            $('.add-currency').css('visibility', 'visible');
        } else {
            $('.add-currency').css('visibility', 'hidden');
        }
    });

    // Событие для кнопки добавить валюту
    $('.add-currency__button').click(function(e) {
        e.stopPropagation();
        var code = $('.add-currency__code').val();
        var name = $('.add-currency__name').val();
        var value = $('.add-currency__value').val();
        if (isNaN(value) == true) {
            alert('В поле "значение" введено не число');
            return;
        }
        if (code == '' || name == '' || value == '') {
            alert('Заполните все поля');
            return;
        }

        newCurr = new NewCurr(code, name, value);
        currencyData.push(newCurr);

        appendCurrCode(code);

        $('.add-currency__success').css('animation', 'visibility 3s ease-in-out');
        $('.add-currency__code').val('');
        $('.add-currency__name').val('');
        $('.add-currency__value').val('');
    });

}

// Функция для расчета результата конвертации
function countResult(state) {
    if (state === 'original') {
        var selectInitClass = '.init-currency__select';
        var selectNewClass = '.new-currency__select';
        var initValClass = '.init-currency__input';
        var newValClass = '.new-currency__input';
    } else {
        var selectInitClass = '.new-currency__select';
        var selectNewClass = '.init-currency__select';
        var initValClass = '.new-currency__input';
        var newValClass = '.init-currency__input';
    }
    var selectInit = $(selectInitClass + ' option:selected').text();
    var selectNew = $(selectNewClass + ' option:selected').text();
    var initVal = $(initValClass).val();
    var initCurVal = currencyData.find(function(item) {
        return item.charCode === selectInit;
    }).value;
    var newCurVal = currencyData.find(function(item) {
        return item.charCode === selectNew;
    }).value;
    var newVal = initVal * initCurVal / newCurVal;

    $(newValClass).attr('value', newVal);
};

$(document).ready(getCurrency('original'));